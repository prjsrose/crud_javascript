/*function bemVindo(){
alert("BEM VINDO! CADASTRE SEUS PRODUTOS!");
}
*/

<form action="javascript:void(0);" method="POST" onsubmit="app.Add()"> 
  <input type="text" id="add-name" placeholder="Novo Produto">
  <input type="submit" value="Adicionar">
</form>

<div id="spoiler" role="aria-hidden">

  <form action="javascript:void(0);" method="POST" id="saveEdit">
    <input type="text" id="edit-name">
    <input type="submit" value="Edit"/> <a onclick="CloseInput()" aria-label="Close">&#10006;</a>
  </form>
</div>

<p id="counter"></p>

<table>
  <tr>
    <th>Produtos:</th>
  </tr>
    <tbody id="countries">
    </tbody>
</table>


var app = new function() {

  this.el = document.getElementById('countries');

  this.countries = ['Arroz', 'Feijão','Leite'];

  this.Count = function(data) {
    var el   = document.getElementById('counter');
    var name = 'country';

    if (data) {
      if (data > 1) {
        name = 'produto(s)';
      }
      el.innerHTML = data + ' ' + name ;
    } else {
      el.innerHTML = 'No ' + name;
    }
  };
  
  this.FetchAll = function() {
    var data = '';

    if (this.countries.length > 0) {
      for (i = 0; i < this.countries.length; i++) {
        data += '<tr>';
        data += '<td>' + this.countries[i] + '</td>';
        data += '<td><button onclick="app.Edit(' + i + ')">Editar</button></td>';
        data += '<td><button onclick="app.Delete(' + i + ')">Deletar</button></td>';
        data += '</tr>';
      }
    }

    this.Count(this.countries.length);
    return this.el.innerHTML = data;
  };

  this.Add = function () {
    el = document.getElementById('add-name');
    // Get valor
    var country = el.value;

    if (country) {
      // Add novo valor
      this.countries.push(country.trim());
      // Reset valor colocar
      el.value = '';
      // Mostra a nova lista
      this.FetchAll();
    }
  };

  this.Edit = function (item) {
    var el = document.getElementById('edit-name');
    // mostra valor
    el.value = this.countries[item];
    // novo valor
    document.getElementById('spoiler').style.display = 'block';
    self = this;

    document.getElementById('saveEdit').onsubmit = function() {
      // Get valor
      var country = el.value;

      if (country) {
        // Edita valor
        self.countries.splice(item, 1, country.trim());
        // mostra nova lista
        self.FetchAll();
        CloseInput();
      }
    }
  };

  this.Delete = function (item) {
    // Delete valor
    this.countries.splice(item, 1);
    //mostrar 
    this.FetchAll();
  };
  
}

app.FetchAll();

function CloseInput() {
  document.getElementById('spoiler').style.display = 'none';
}